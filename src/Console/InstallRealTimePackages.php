<?php

namespace Roynex\RealTimePackage\Console;

use Illuminate\Console\Command;

class  InstallRealTimePackages extends Command
{

    protected $signature = 'RealTime:install';

    protected $description = 'Install RealTime Dependencies';

    public function handle()
    {

        $this->info('first of all please dont forget to initial redis in config/database');

        $this->info('in .env add redis as brodcast driver');

        $this->info('dont forget to unComment registering BrodcastServiceProvider in providers array at config/app');

        if ($this->confirm('Do you wish to continue?'))
        {
        $bar = $this->output->createProgressBar(12);

        $bar->start();

        $this->info('Installing RealTime Packages...');

       /* $this->info('restarting queue statues....');

        $this->call('queue:restart');

        $bar->advance();

        $this->info('running redis queue...');

        system('nohup php artisan queue:work redis --daemon &');

        $bar->advance();

        $this->info('running database queue....');

        system('nohup php artisan queue:work database --daemon &');

        $bar->advance();*/

        $this->info('installing npm dependencies...');

        $this->info('installing laravel echo server');

        system('npm install -g laravel-echo-server');

        $bar->advance();

        $this->info('install general dependencies....');

        system('npm install');

        $bar->advance();

        $this->info('installing laravel echo....');

        system('npm install laravel-echo');

        $bar->advance();

        $this->info('installing socket io client....');

        system('npm install socket.io-client');

        $bar->advance();

        $this->info('installing redis server....');

        system('sudo apt install redis-server');

        $bar->advance();

        $this->info('running npm dev....');

        system('npm run dev');

        $this->info('initiating laravel echo server....');

        system('laravel-echo-server init');

        $bar->advance();

        $this->info('running laravel echo server...');

        system('nohup laravel echo server run --daemon &');

        $bar->advance();

        $this->info('publishing main event class...');

        $this->call('vendor:publish --tag=event --force');

        $bar->advance();

        $this->info('publishing general jobs');

        $this->call('vendor:publish --tag=WebGroupNotificationJob --force');

        $this->info('...');

        $bar->advance();

        $this->call('vendor:publish --tag=WebNotificationJob --force');

        $this->info('...');

        $bar->advance();

        $this->call('vendor:publish --tag=WebUsersGroupNotification --force');

        $this->info('...');

        $bar->advance();

        $this->info('installing RealTime finished successfully');

        $this->info('dont forget to register brodcast routes in BrodcastServiceProvider');

        $this->info('dont forget to add Channel user-channel in routes/channel');

        $bar->finish();
        }

    }

}