<?php


namespace Roynex\RealTimePackage\Events;


use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GeneralEvent
{
    use Dispatchable,SerializesModels;

    protected $title ,$description ,$data ,$userId ,$type ,$roleId;


    /**
     * Create a new event instance.
     *
     * @param $description
     * @param $data
     * @param $type
     * @param $userId
     * @param null $roleId
     */
    public function __construct($description ,$data ,$type,$userId,$roleId = null)
    {
        //ToDO:: change title  to your project Name

        $this->title = 'your project title';
        $this->description = $description;
        $this->data = $data;
        $this->userId = $userId;
        $this->type = $type;
        $this->roleId = $roleId;
    }



    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('user-channel.'.$this->userId);
    }

    public function broadcastWhen()
    {
        //ToDO:: change role id to your admin role id

        return empty($this->roleId) || $this->roleId == 2;
    }

    public function broadcastAs()
    {
        return 'GeneralEvent';
    }

    public function broadcastWith()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'data' => $this->data,
            'type' => $this->type,
        ];
    }

}