<?php

namespace App\Jobs;

use App\Events\GeneralEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WebNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $description ,$data ,$userId ,$type;

    /**
     * Create a new job instance.
     *
     * @param $description
     * @param $type
     * @param $data
     * @param $userId
     */
    public function __construct($description ,$type ,$data ,$userId)
    {
        $this->description = $description;
        $this->type = $type;
        $this->data = $data;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        event(new GeneralEvent($this->description ,$this->data ,$this->type ,$this->userId));
    }
}
