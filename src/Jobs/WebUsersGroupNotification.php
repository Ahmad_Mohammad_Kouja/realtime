<?php

namespace App\Jobs;

use App\Events\GeneralEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WebUsersGroupNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $userIds ,$description ,$type ,$typeId ,$roleId;

    /**
     * Create a new job instance.
     *
     * @param $userIds
     * @param $description
     * @param $type
     * @param $typeId
     * @param null $roleId
     */
    public function __construct($userIds , $description ,$type ,$typeId ,$roleId = null)
    {
        $this->userIds = $userIds;
        $this->description = $description;
        $this->type = $type;
        $this->typeId  = $typeId;
        $this->roleId = $roleId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->userIds as $userId)
            event(new GeneralEvent($this->description,$this->typeId,$this->type,$userId,$this->roleId));
    }
}
