<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WebCompanyGroupNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $companyId ,$title ,$type ,$data ,$userId ,$description;

    /**
     * Create a new job instance.
     *
     * @param $companyId
     * @param $description
     * @param $type
     * @param $data
     * @param $userId
     */
    public function __construct($companyId,$description,$type,$data,$userId)
    {
        $this->companyId = $companyId;
        $this->data = $data;
        $this->type = $type;
        $this->userId = $userId;
        $this->description = $description;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(User $user)
    {
        $companyUsers = $user->getData(function ($users)
        {
            $users->where('company_id',$this->companyId)
                ->where('id','!=',$this->userId);
        });
        foreach ($companyUsers as $companyUser)
            dispatch(new WebNotification($this->description,$this->type,$this->data,$companyUser->id));
    }
}
