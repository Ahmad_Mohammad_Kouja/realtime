<?php


namespace Roynex\RealTimePackage;


use Illuminate\Support\ServiceProvider;
use Roynex\RealTimePackage\Console\InstallRealTimePackages;

class RealTimeServiceProvider extends ServiceProvider
{

    public function register()
    {

    }

    public function boot()
    {

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallRealTimePackages::class,
            ]);
        }

        $this->publishes([
            __DIR__.'/Events/GeneralEvent.php' => app_path('Events/GeneralEvent.php')
        ], 'event');

        $this->publishes([
            __DIR__.'/Jobs/WebCompanyGroupNotification.php' => app_path('Jobs/WebCompanyGroupNotification.php')
        ], 'WebGroupNotificationJob');

        $this->publishes([
            __DIR__.'/Jobs/WebNotification.php' => app_path('Jobs/WebNotification.php')
        ], 'WebNotificationJob');

        $this->publishes([
            __DIR__.'/Jobs/WebUsersGroupNotification.php' => app_path('Jobs/WebUsersGroupNotification.php')
        ], 'WebUsersGroupNotification');

        $this->publishes([
            __DIR__.'/Console/InstallRealTimePackages.php' => app_path('Console/InstallRealTimePackages.php')
        ], 'RealTimePackageCommand');
    }
}