<?php


namespace Roynex\RealTimePackage\Tests\UnitTest;


use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Roynex\RealTimePackage\Tests\TestCase;

class InstallRealTImePackageTest extends TestCase
{

    function the_install_command_copies_the_configuration()
    {
        // make sure we're starting from a clean state
        if (File::exists(config_path('RealTimePackage.php'))) {
            unlink(config_path('RealTimePackage.php'));
        }

        $this->assertFalse(File::exists(config_path('RealTimePackage.php')));

        Artisan::call('RealTime:install');

        $this->assertTrue(File::exists(config_path('RealTimePackage.php')));
    }

}